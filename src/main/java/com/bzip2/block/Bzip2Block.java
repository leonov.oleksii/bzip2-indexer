package com.bzip2.block;

public class Bzip2Block {

    private final int uncompressedDataSize;
    private final long compressedByteOffset;
    private final byte compressedBitOffset;

    public Bzip2Block(int uncompressedDataSize, long compressedByteOffset, byte compressedBitOffset) {
        this.uncompressedDataSize = uncompressedDataSize;
        this.compressedByteOffset = compressedByteOffset;
        this.compressedBitOffset = compressedBitOffset;
    }

    public int getUncompressedDataSize() {
        return uncompressedDataSize;
    }

    public long getCompressedByteOffset() {
        return compressedByteOffset;
    }

    public byte getCompressedBitOffset() {
        return compressedBitOffset;
    }

    @Override
    public String toString() {
        return "Bzip2Block{" + "dataSize=" + uncompressedDataSize +
                ", byteOffset=" + compressedByteOffset +
                ", bitOffset=" + compressedBitOffset +
                '}';
    }
}
