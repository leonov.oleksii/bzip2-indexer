package com.bzip2.block.event;

import com.bzip2.block.Bzip2Block;

import java.util.EventListener;

public interface Bzip2BlockListener extends EventListener {
    void blockWritten(Bzip2Block block);
}
